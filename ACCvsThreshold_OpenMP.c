/*
 * ACCvsThreshold_MPI.c
 *
 *  Created on: May 4, 2011
 *      Author: zahoo
 *      Email: gzahoranszky@gmail.com
 *
 *      Version: 1.0.0
 *
 *      Depends:
 *
 *			    math.h (Mathematics, standard C)
 *			    string.h (String, standard C)
 *      		bamopenmp.h (Binary Adjacency Matrix using OPENMPI, version 1.0.0)
 *      		graphopopenmp (Graph Operations, version 1.0.0, depends on bampi.h version 1.0.0)
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "phd_c.h"
#include "bamopenmp.h"
#include "graphopopenmp.h"

#ifdef _OPENMP
#include <omp.h>
#endif



// # PROTOTYPE FUNCTIONS - START
void writeACCResult (double, double, double, int, char *, int, int, char *, int);
// # PROTOTYPE FUNCTIONS - END


int main(int argc, char *argv[])
{
	struct bitpositions *bp;
	struct binary_adjacency_matrix *BAM;
	double *CC;
	double ACC = 0.0;
	double SAMPLESTDDEVACC = 0.0;
	double threshold;
	int *NodesOfDegreeMoreThanZero;
	int QualifiedNodes = 0;
	char *filename_in;
	char *expid;
	long int NODL = 0;
	int maxnodeidx = 0;
	int nrthreads = 0;
	int i = 0;
//	int j = 0;
	int useweights = 0;
	int checker = 0;
	int thread_nr = 0;				// # number of threads to be used


//	if ((argc > 7) || (argc < 7))									// # number of arguments must be 4
	if (argc < 7)									// # number of arguments must be 4

	{
		fprintf(stderr, "\n<SYNTAX>\n%s <input file> <threshold> <maximal vertex ID> <number of threads> <experiment ID> <use edge weights: 1 yes, 0 no>\n\n", argv[0]);

		exit (61);
	}
	// # validating inputs


	/* input filename */
	for (checker = 0; argv[1][checker] > 0; checker++)		// # validating input file's name
		;
	if (checker > 255)
		serror (56);

	checker = 0;														// # reset temporary variable


	/* threshold */
	for (checker = 0; argv[2][checker] > 0; checker++)		// # validating the threshold.
		;																	// # maximum allowed length is 7 characters
	if (checker > 7)
		serror (65);

	checker = 0;														// # reset temporary variable


	/* maximal ID */
	for (checker = 0; argv[3][checker] > 0; checker++)		// # validating the maximal ID.
		;																	// # maximum allowed length is 7 characters
	if (checker > 7)
		serror (74);

	checker = 0;														// # reset temporary variable



	/* number of threads */
	for (checker = 0; argv[4][checker] > 0; checker++)		// # validating the number of threads
		;																	// # maximum allowed length is 7 characters
	if (checker > 7)
		serror (130);

	checker = 0;														// # reset temporary variable


	/* experiment ID */
	for (checker = 0; argv[5][checker] > 0; checker++)		// # validating the experiment ID
		;																	// # maximum allowed length is 7 characters
	if (checker > 255)
		serror (139);

	checker = 0;														// # reset temporary variable


	/* use weights */
	for (checker = 0; argv[6][checker] > 0; checker++)		// # validating the use weights
		;																	// # maximum allowed length is 1 characters
	if (checker > 1)
		serror (108);

	checker = 0;														// # reset temporary variable


	filename_in = argv[1];
	threshold = (double) atof (argv[2]);
	maxnodeidx = atoi (argv[3]);
	thread_nr = atoi (argv[4]);
	expid = argv[5];
	useweights = atoi (argv[6]);


	if ((useweights != 0) && (useweights != 1))
		serror (1);


	NODL = get_number_of_lines (filename_in);

	fprintf (stdout, "################################################################################\n");
	fprintf (stdout, "Maximal vertex ID: %d\n", maxnodeidx);
	fprintf (stdout, "Number of datalines: %ld\n", NODL);
	fprintf (stdout, "Thread number: %d\n", thread_nr);

#ifdef _OPENMP
	fprintf(stdout, "number of processors: %d\n",omp_get_num_procs());
	omp_set_num_threads(thread_nr);
	fprintf(stdout, "number of threads used: %d\n",omp_get_max_threads());
#else
	fprintf(stderr, 	"not compiled with OpenMP!\n");
	thread_nr = 1;
#endif
	fprintf (stdout, "################################################################################\n");


	CC = (double *) calloc (maxnodeidx, sizeof (double));
	NodesOfDegreeMoreThanZero =  (int *) calloc (maxnodeidx, sizeof (int));



	bp = initBitPositions (maxnodeidx);
	generateBitPositions (bp);


	BAM = init_binary_adjacency_matrix (maxnodeidx, maxnodeidx);
	buildBinaryAdjacencyMatrix (BAM, filename_in, NODL, useweights, bp);

//	showBinaryAdjacencyMatrix (BAM);


//  showBitPositions (bp);

	QualifiedNodes = getNodesOfDegreeMoreThanZero (BAM, NodesOfDegreeMoreThanZero, bp);


	#pragma omp parallel private (i) shared (maxnodeidx, CC, BAM, bp, NodesOfDegreeMoreThanZero, QualifiedNodes)
	{
		#pragma omp for schedule (dynamic)
		for (i = 0; i < BAM->rownr; i++) {

			if (i % 100 == 0)
				fprintf (stdout, "Node %d done.\n", i + 1);

			CC[i] = getCC (BAM, i + 1, maxnodeidx, bp);
		}
	}



	free_binary_adjacency_matrix (BAM);
	freeBitPositions (bp);
	ACC = computeACC (CC, maxnodeidx, NodesOfDegreeMoreThanZero);
	SAMPLESTDDEVACC = computeSAMPLESTDDEVACC (CC, maxnodeidx, ACC, NodesOfDegreeMoreThanZero);
	writeACCResult (threshold, ACC, SAMPLESTDDEVACC, QualifiedNodes, filename_in, nrthreads, maxnodeidx, expid, useweights);

	free(CC);
	CC = NULL;

	free(NodesOfDegreeMoreThanZero);
	NodesOfDegreeMoreThanZero = NULL;

	return 0;
}



void writeACCResult (double threshold, double ACC, double SAMPLESTDDEVACC, int QualifiedNodes, char *filename_in, int nrthreads, int maxnodeidx, char *expid, int useweights) {
	FILE *fpout;
	char *filename_out;
	int newsize = 0;

	newsize = strlen (filename_in);

	if (newsize > 239)
		serror (55000);

	filename_out = (char *) calloc (newsize+15, sizeof (char));
	sprintf (filename_out, "%s.acc.result", filename_in);

	fpout = fopen (filename_out, "w");

	fprintf (fpout, "#Program:\tACCvsThreshold_OpenMP.c\n#Version\t1.0.0\n#Command line:\t\n");
	fprintf (fpout, "#./ACCvsThreshold_OpenMP %s %lf %d %d %s %d\n", filename_in, threshold, maxnodeidx, nrthreads, expid, useweights);
	fprintf (fpout, "#The two thread number in commandline are supposed to have the same value. Since the mpi part of command line is not passed to the code the number after the -np is only assumed. Make sure, they have the same valus at execution!.\n");
	fprintf (fpout, "#treshold\tACC\tSAMPLESTDDEV(CC)\tNrQoalifiedNodes\n");
	fprintf (fpout, "# <DATA/>\n");
	fprintf (fpout, "%lf\t%lf\t%lf\t%d\n", threshold, ACC, SAMPLESTDDEVACC, QualifiedNodes);
	fprintf (fpout, "# <\\DATA>\n");

	fclose(fpout);

	free (filename_out);
	filename_out = NULL;
}
