/*
 * bamopenmp.h
 * The binary adjacency matrix refers to the adjacency matrix represented by bytes. Thus
 * the binary does not refer to the values 1 and 0 used in the matrix but to the
 * way of implementation.
 *
 *  Created on: May 4, 2010
 *  	Version: 1.0.0
 *      Author: zahoo
 *
 *	Depends:
 *			    math.h	(Mathematics, standard C )
 *				phd_c	version 1.0.1, common functions, like error message sending
 *
 *	Version: 1.0.0
 */



#ifndef BAMOPENMP_H_
#define BAMOPENMP_H_

#include <math.h>
#include "phd_c.h"

// # STRUCTURES - START
struct binary_adjacency_matrix {
	unsigned char **bam;
	int userrownr;			// This is what the user want, i.e. number of rows. The final
						// number of rows is the same.
	int usercolnr;			// This is what the user want, i.e. number of columns.
	int rownr;	// This is the same as userrownr
	int colnr;	// This is computed based on usercolnr and gives the number of bytes used
				// to represent one row of the adjacency matrix. This will be the multiple
				// of 8.
	int colbitnr;	// This is actually 8 x colnr.

};

struct bitpositions {
	int maxvertexID;
	struct bitposition *bp;
};


struct bitposition {
	int vertexID;
	int blockindex;
	int bit;
};

struct Edge {
	int nodeA;
	int nodeB;
};

struct EdgeList {
	long int edgenr;
	struct Edge *edges;
};

// # STRUCTURES - END


// # PROTOTYPE FUNCTIONS - START
struct binary_adjacency_matrix *init_binary_adjacency_matrix (int, int);
void free_binary_adjacency_matrix (struct binary_adjacency_matrix *);
void setBitOn (struct binary_adjacency_matrix *, int, int, struct bitpositions *);
void buildBinaryAdjacencyMatrix (struct binary_adjacency_matrix *, char *, int, int, struct bitpositions *p);
void showBinaryAdjacencyMatrix (struct binary_adjacency_matrix *);
int isBitOn (struct binary_adjacency_matrix *, int, int, struct bitpositions *);
int isBitOnInUnsignedCharArray (unsigned char *, int, int, struct bitpositions *);
struct bitpositions *initBitPositions (int);
void generateBitPositions (struct bitpositions *);
void showBitPositions (struct bitpositions *);
void freeBitPositions (struct bitpositions *);
long int get_number_of_lines (char *);
struct EdgeList *initEdgeList (struct EdgeList *, long int);
void freeEdgeList (struct EdgeList *);
void parseEdgeList (struct EdgeList *, char *, int);	// checked, parsing OK
void showEdgeList (struct EdgeList *);
void resetUnsignedCharVector (unsigned char *, int);
void compileMessage (unsigned char *, int, struct binary_adjacency_matrix *);
// # PROTOTYPE FUNCTIONS - END

struct binary_adjacency_matrix *init_binary_adjacency_matrix (int rownr, int colnr) {
	struct binary_adjacency_matrix *B;

	double num = 0.0;
	double a,b = 0.0;

	int i = 0;

	B = (struct binary_adjacency_matrix *) calloc (1,sizeof (struct binary_adjacency_matrix));
	if (B == NULL)
		serror (6300);

	B->userrownr = rownr;
	B->rownr = B->userrownr;
	B->usercolnr = colnr;
	B->bam = (unsigned char **) calloc (B->rownr, sizeof (unsigned char*));

	if (B->bam == NULL)
		serror (7100);

	// computing the necessary number of char variables
	if (B->usercolnr % 8 == 0)
		B->colnr = (int) (B->usercolnr / 8);
	else {
		num = (double) (B->usercolnr / 8);
		a = modf (num, &b);
		B->colnr = (int) (b + 1);
	}

	// computing the number of bits included by one row of the binary adjacency matrix
	B->colbitnr = B->colnr * 8;

	for (i = 0; i < rownr; i++) {
		B->bam[i] = (unsigned char *) calloc (B->colnr, sizeof (unsigned char));
		if (B->bam[i] == NULL)
			serror (8800);
	}

	return B;
}


void free_binary_adjacency_matrix (struct binary_adjacency_matrix *BAM) {
	int i = 0;

	for (i = 0; i < BAM->userrownr; i++) {
		free (BAM->bam[i]);
		BAM->bam[i] = NULL;
	}

	free (BAM->bam);
	BAM->bam = NULL;

	free (BAM);
	BAM = NULL;

}


void setBitOn (struct binary_adjacency_matrix *BAM, int i, int j, struct bitpositions *p) {
	int colnr = 0;
	unsigned char bit = 0;
	unsigned char binary[8];
	int bitposition = 0;

	binary[0] = 1;
	binary[1] = 2;
	binary[2] = 4;
	binary[3] = 8;
	binary[4] = 16;
	binary[5] = 32;
	binary[6] = 64;
	binary[7] = 128;



	if ((i < 1) || (j < 1))
		serror (1110);

	if (i > BAM->userrownr)
		serror(1130);

	if (j > BAM->usercolnr)
		serror(1160);

	i--;
	j--;


	colnr = p->bp[j].blockindex;
	bitposition = p->bp[j].bit;
//	fprintf (stdout, "bit = %d value: %d\n", bit, (int) binary[bit]);
	bit = binary[bitposition];
//	fprintf (stdout, "before: row: %d  col: %d  cell value: %d\n", i, colnr, (int) BAM->bam[i][colnr]);
	BAM->bam[i][colnr] = BAM->bam[i][colnr] | bit;
//	fprintf (stdout, "after: row: %d  col: %d  cell value: %d\n", i, colnr, (int) BAM->bam[i][colnr]);


}

void buildBinaryAdjacencyMatrix (struct binary_adjacency_matrix *BAM, char *fname, int NODL, int useweights, struct bitpositions *p) {
// # NODL: number of data lines

	FILE *fp;
	int i = 0;
	int id1 = 0;
	int id2 = 0;
	int newid = 0;
	double sim = 0.0;

	fp = fopen (fname, "r");


	for (i = 0; i < NODL; i++)
	{
		id1 = 0;
		id2 = 0;
		if (useweights == 0) {
			newid = 0;
			fscanf (fp, "%d %d\n", &id1, &id2);
			setBitOn (BAM, id1, id2, p);
			setBitOn (BAM, id2, id1, p);
		}
		else if (useweights == 1){
			fscanf (fp, "%d %d %lf\n", &id1, &id2, &sim);
			setBitOn (BAM, id1, id2, p);
			setBitOn (BAM, id2, id1, p);
		}
		else
			serror (213000);
	}


	fclose (fp);
}


void showBinaryAdjacencyMatrix (struct binary_adjacency_matrix *BAM) {
	int i = 0;
	int j = 0;

	for (i = 0; i < BAM->rownr; i++) {
		for (j = 0; j < BAM->colnr; j++) {
			fprintf (stdout, "%d ", (int) BAM->bam[i][j]);
		}
		fprintf (stdout, "\n");
	}
}

int isBitOn (struct binary_adjacency_matrix *BAM, int i, int j, struct bitpositions *p) {
//	int rownr = 0;
	int colnr = 0;
	int bitposition = 0;
//	unsigned char bit = 0;
	unsigned char compare = 1;
	unsigned char result = 0;
	unsigned char tempval = 0;


//	fprintf (stdout, "looking: row: %d   col: %d\n", i, j);

	if ((i < 1) || (j < 1))
		serror (1110);

	if (i > BAM->userrownr)
		serror(1130);

	if (j > BAM->usercolnr)
		serror(1160);

	i--;
	j--;


	colnr = p->bp[j].blockindex;
	bitposition = p->bp[j].bit;
//	fprintf (stdout, "i: %d, j: %d blockindex: %d bit: %d\n", i, j, colnr, bit);

	if (bitposition == 0) {
		result = BAM->bam[i][colnr] & compare;

		if (result == 1) {
//			fprintf (stdout, "hit: row: %d   col: 0\n", i + 1);
			return 1;
		}
	}
	else {
//		fprintf (stdout, "bit = %d\n value: %d\n", bit, (int) binary[bit]);
		tempval = BAM->bam[i][colnr] >> bitposition;

		result = tempval & compare;
//		fprintf (stdout, "tempval : %d, origin val: %d compare: %d, result: %d\n", (int) tempval, (int) BAM->bam[i][colnr], (int) compare, (int) result);
		if (result == 1) {
//			fprintf (stdout, "hit: row: %d   col: %d\n", i + 1, bitposition);
			return 1;
		}
	}
//	fprintf (stdout, "no hit\n");
	return 0;
}


int isBitOnInUnsignedCharArray (unsigned char *NeighborVector, int j, int maxnodeidx, struct bitpositions *p) {
//	int rownr = 0;
	int colnr = 0;
	int bitposition = 0;
//	unsigned char bit = 0;
	unsigned char compare = 1;
	unsigned char result = 0;
	unsigned char tempval = 0;


//	fprintf (stdout, "looking: row: %d   col: %d\n", i, j);

	if (j < 1)
		serror (21110);


	if (j > maxnodeidx)
		serror(21160);

	j--;


	colnr = p->bp[j].blockindex;
	bitposition = p->bp[j].bit;
//	fprintf (stdout, "i: %d, j: %d blockindex: %d bit: %d\n", i, j, colnr, bit);

	if (bitposition == 0) {
		result = NeighborVector[colnr] & compare;

		if (result == 1) {
//			fprintf (stdout, "hit: row: %d   col: 0\n", i + 1);
			return 1;
		}
	}
	else {
//		fprintf (stdout, "bit = %d\n value: %d\n", bit, (int) binary[bit]);
		tempval = NeighborVector[colnr] >> bitposition;

		result = tempval & compare;
//		fprintf (stdout, "tempval : %d, origin val: %d compare: %d, result: %d\n", (int) tempval, (int) BAM->bam[i][colnr], (int) compare, (int) result);
		if (result == 1) {
//			fprintf (stdout, "hit: row: %d   col: %d\n", i + 1, bitposition);
			return 1;
		}
	}
//	fprintf (stdout, "no hit\n");
	return 0;
}

struct bitpositions *initBitPositions (int maxvertexID) {
	struct bitpositions *p;

	if (maxvertexID < 1)
		serror (30200);

	p = (struct bitpositions *) calloc (1, sizeof (struct bitpositions));
	if (p == NULL)
		serror (30600);

	p->maxvertexID = maxvertexID;

	p->bp = (struct bitposition *) calloc (maxvertexID, sizeof (struct bitposition));
	if (p->bp == NULL)
		serror (31200);

	return p;
}


void generateBitPositions (struct bitpositions *p) {
	int j = 0;
	int colnr = -1;
	int bit = -1;
	double a,b = -1.0;
	int maxvertexID = p->maxvertexID;

	for (j = 0; j < maxvertexID; j++) {
		colnr = -1;
		bit = -1;
		if ((j % 8) == 0) {
			colnr = (int) (j / 8);
			bit = 0;
		}
		else {
			a = modf((j / 8), &b);
			colnr = (int) b;
			bit = (int) (j - (b * 8));
		}
		if ((colnr < 0) || (bit < 0))
			serror (31300);
		else
			p->bp[j].vertexID = j + 1;
			p->bp[j].blockindex = colnr;
			p->bp[j].bit = bit;
	}
}

void showBitPositions (struct bitpositions *p) {
	int j = 0;

	for (j = 0; j < p->maxvertexID; j++)
		fprintf (stdout, "vertexID: %d, blockindex: %d, bit: %d\n", p->bp[j].vertexID, p->bp[j].blockindex, p->bp[j].bit);
}

void freeBitPositions (struct bitpositions *p) {

	free (p->bp);
	p->bp = NULL;
	free (p);
	p = NULL;
}


long int get_number_of_lines (char *file_in) {
        FILE *fp;
        const int endofline = CB;       // # end of line character
        char i = 0;
        long int linecounter = 0;

        fp = fopen (file_in, "r");

        while (i != EOF)        // # read until the end of line
        {
                i = (char) fgetc (fp);
                if (i == endofline)                     // # if a | char is found, then
                        linecounter++;
//                      fprintf(stdout,"+");            // # increase sepcounter by 1
        }

        fclose (fp);

        return linecounter;
}

struct EdgeList *initEdgeList (struct EdgeList *el, long int edgenr) {

	el = (struct EdgeList *) calloc (1, sizeof (struct EdgeList));
	el->edgenr = edgenr;
	el->edges = (struct Edge *) calloc (el->edgenr, sizeof(struct Edge));

	return el;
}

void freeEdgeList (struct EdgeList *el) {
	free(el->edges);
	el->edges = NULL;
	free(el);
	el = NULL;
}

void parseEdgeList (struct EdgeList *el, char *fname, int useweights) {
	FILE *fp;
	double sim = 0.0;

	long int i = 0;

	fp = fopen (fname, "r");
	for (i = 0; i < el->edgenr; i++) {
		if (useweights == 0)
			fscanf (fp, "%d %d\n", &el->edges[i].nodeA, &el->edges[i].nodeB);
		else if (useweights == 1)
			fscanf (fp, "%d %d %lf\n", &el->edges[i].nodeA, &el->edges[i].nodeB, &sim);
		else
			serror (48900);
	}
	fclose (fp);

}

void showEdgeList (struct EdgeList *el) {
	long int i = 0;

	for (i = 0; i < el->edgenr; i++)
		fprintf(stdout, "%d %d\n", el->edges[i].nodeA, el->edges[i].nodeB);

}


void resetUnsignedCharVector (unsigned char *vector, int length) {
	int i = 0;

	for (i = 0; i < length; i++)
		vector[i] = 0;

}

void compileMessage (unsigned char *message, int messagesize, struct binary_adjacency_matrix *BAMvector) {
	int i = 0;

	if (BAMvector->colnr != messagesize)
		serror (46300);

	for (i = 0; i < BAMvector->colnr; i++)
		message[i] = BAMvector->bam[0][i];

}


#endif /* BAMOPENMP_H_ */
