/*
 * graphopopenmp.h
 *
 *  Created on: May 4, 2011
 *      Author: zahoo
 *      Email: gzahoranszky@gmail.com
 *
 *		Version: 1.0.0
 *
 *	graphops stands for Graph Operations.
 *	Depends:
 *			 bamopenmp.h	version 1.0.0 which is a Binary Adjacency Matrix for MPI header file.
 *			 phd_c		version 1.0.1, common functions, like error message sending
 */

#ifndef GRAPHOPOPENMP_H_
#define GRAPHOPOPENMP_H_

#include "bamopenmp.h"
#include "phd_c.h"



// # PROTOTYPE FUNCTIONS - START
void getNeighbors (struct binary_adjacency_matrix *, struct bitpositions *, struct EdgeList *, int);
int getDegree (unsigned char *, int, struct bitpositions *);
long int getNrCommonNeighbors (unsigned char *, int, unsigned char *);
double computeCC (int, long int);
double computeACC (double *, int, int*);
double computeSAMPLESTDDEVACC (double *, int, double, int*);
double getCC (struct binary_adjacency_matrix *, int, int, struct bitpositions *);
int getNodesOfDegreeMoreThanZero (struct binary_adjacency_matrix *, int *, struct bitpositions *);
// # PROTOTYPE FUNCTIONS - END



void getNeighbors (struct binary_adjacency_matrix *BAMvector, struct bitpositions *bp, struct EdgeList *el, int nodeid) {
	long int i = 0;

	// reset vector
	resetUnsignedCharVector (BAMvector->bam[0], BAMvector->colnr);
//	for (j = 0; j < BAMvector->colnr; j++)
//		BAMvector->bam[0][j] = 0;

	for (i = 0; i < el->edgenr; i++) {
		if (el->edges[i].nodeA == nodeid)
			setBitOn (BAMvector,1,el->edges[i].nodeB, bp);
		else if (el->edges[i].nodeB == nodeid)
			setBitOn (BAMvector,1,el->edges[i].nodeA, bp);
	}
}


int getDegree (unsigned char *NeighborVector, int maxnodeidx, struct bitpositions *p) {
	int degree = 0;
	int i = 0;
	int res = 0;

	for (i = 0; i < maxnodeidx; i++) {
		res = 0;
		res = isBitOnInUnsignedCharArray (NeighborVector, i + 1, maxnodeidx, p);
//		fprintf (stdout, "(midx %d) %d", maxnodeidx,res);
//		fflush (stdout);
		if (res == 1) {
//			fprintf (stdout, "HIT: %d\n", i + 1);
			degree++;
		}
	}
//	fprintf (stdout, "\n");
//	fflush (stdout);


	if (degree < 0)
		serror (64);

//	fprintf (stdout, "D: %d\n", degree);
	return degree;
}



long int getNrCommonNeighbors (unsigned char *message, int messagesize, unsigned char *actualnodeadjvector) {

	unsigned char *result;
	unsigned char compare = 1;

	long int nrofcommonneighbors = 0;
	int i = 0;
	int j = 0;


	result = (unsigned char *) calloc (messagesize, sizeof(unsigned char));


	//	Doing AND bitwise operation on the neighbor vectors. Each bit, referring to a common neighbor will represented by 1.
	//	All other bits will be 0, because of the nature of AND operation. No need for testing if a neighbor vector is the
	//	investigated guy's vector, because before invoking this function it is tested whether this vector contains the guy.
	//	Clearly, the guy cannot contain itself in its own neighbor vector, so actually it won't be considered for looking for
	//	common neighbors.
	for (i = 0; i < messagesize; i++)
		result[i] = message[i] & actualnodeadjvector[i];


	//	Counting the number of 1 is the resultant bitvector, which yields the number of common neighbors.
	for (i = 0; i < messagesize; i++) {
		result[i] = message[i] & actualnodeadjvector[i];
//		fprintf (stdout, "[%d - %d]: %d %d --> %d\n", master, secondary, (int) message[i], (int) actualnodeadjvector[i], (int) result[i]);
		//	counting ones with help of right shift and bitwise AND operation with 00000001 vector. If the last bit is 1 of the result, then we counted a bit.
		for (j = 0; j < 8; j++) {
			if ((result[i] & compare) == 1) {
				nrofcommonneighbors++;
//				fprintf (stdout, "HIT: [%d %d]\n", i + 1, j + 1);
//				fflush (stdout);
			}
			result[i] >>= 1;	//	shift bits to the right by 1.
		}
	}


	free(result);
	result = NULL;


	return nrofcommonneighbors;
}


double computeCC (int degree, long int NeighborEdges) {
	double CC = 0.0;
	double edgenr = 0.0;
	double possibleedgenr = 0.0;
//	Due to the implemetation of counting the number of common neighbors of neighbors, the value of TotalNeighborEdges is actually twice as much as
//	the real number of edges between the neighbors of a node. Therefore it will be divided by two.

	possibleedgenr = (double) (degree * (degree - 1) / 2);


	if (degree == 0)
		CC = 0.0;
	else if (degree == 1)
		CC = 0.0;
	else if (degree > 1) {
		if (NeighborEdges % 2 != 0)
			serror (1230000);
		else {
			edgenr = (double) (NeighborEdges / 2);
			CC = (double) (edgenr / possibleedgenr);
		}
	}
	else
		serror (1222222);

	if ((CC < 0) || (CC > 1))
		serror (1250000);

	return CC;
}

double computeACC (double *CC, int maxnodeidx, int *NodesOfDegreeMoreThanZero) {
	int i = 0;
	double Sum = 0.0;
	double ACC = 0.0;
	int QualifiedNodeNr = 0;

	for (i = 0; i < maxnodeidx; i++) {
		if (NodesOfDegreeMoreThanZero[i] > 0) {
			Sum += CC[i];
			QualifiedNodeNr++;
		}
	}

	if (QualifiedNodeNr != 0)
		ACC = (double) (Sum / QualifiedNodeNr);
	else
		ACC = 0;

	return ACC;

}

double computeSAMPLESTDDEVACC (double *CC, int maxnodeidx, double ACC, int *NodesOfDegreeMoreThanZero) {
	int i = 0;
	double Sum = 0.0;
	double SAMPLESTDDEVACC = 0.0;
	int QualifiedNodeNr = 0;

	for (i = 0; i < maxnodeidx; i++) {
//		Sum += ((CC[i]-ACC) * (CC[i]-ACC));
			if (NodesOfDegreeMoreThanZero[i] > 0) {
				QualifiedNodeNr++;
				Sum += pow((CC[i]-ACC),2);
			}
	}

	SAMPLESTDDEVACC = (double) (sqrt (Sum / (QualifiedNodeNr - 1)));	//	due to the -1 term it is a SampleStandardDeviation and not Standard deviation.

	return SAMPLESTDDEVACC;

}


double getCC (struct binary_adjacency_matrix *BAM, int masternodeid, int maxnodeidx, struct bitpositions *bp) {
	int degree = 0;
	int i = 0;
	int j = 0;
	double CC = 0.0;
	long int NeighborEdges = 0;

	// ************* Preparing message ********************************************* //
	// ** sending the adjacency row of node i + 1 ********************************** //
	// ***************************************************************************** //
//			resetUnsignedCharVector (messenger,messagesize);
//			getNeighbors (NeighborVector,bp,el, i + 1);
//			compileMessage (messenger, messagesize, NeighborVector);
	// ***************** Message ready ********************************************* //


	if (masternodeid < 1)
		serror (250000);
	if (masternodeid > maxnodeidx) {
//		fprintf (stdout, "Nodeid higher than maximum index %d, %d\n", masternodeid, maxnodeidx);
//		fflush(stdout);
		serror (252000);
	}

	i = masternodeid - 1;


	degree = getDegree (BAM->bam[i], BAM->usercolnr, bp);
	NeighborEdges = 0;
	for (j = 0; j < BAM->rownr; j++) {
//			  Dnrcommonneighbors = 0.0;
//			  fprintf (stdout, "? Node to compare %d... current node: %d (master)\n", ((z + 1) + ((rank - 1) * unit)), i + 1);
//			  fprintf (stdout, "? %d - %d\n", i + 1, (z + 1) + ((rank - 1) * unit));
		if ((i != j) && (isBitOnInUnsignedCharArray (BAM->bam[j], i + 1, maxnodeidx, bp) == 1)) {
			NeighborEdges += getNrCommonNeighbors (BAM->bam[i], BAM->colnr, BAM->bam[j]);
//				fprintf (stdout, "%ld\n", NeighborEdges);
//			  fprintf (stdout, "Comparing nodes neighbors vector %d (master) and %d... sum of nr of common neighbors = %ld\n", i + 1, (z + 1) + ((rank - 1) * unit), NeighborEdges);

		}
	}
	CC = computeCC (degree,NeighborEdges);


	return CC;
}



int getNodesOfDegreeMoreThanZero (struct binary_adjacency_matrix *BAM, int *NodesOfDegreeMoreThanZero, struct bitpositions *bp) {
	int i = 0;
	int degree = 0;
	int res = 0;
	int NrNodesOfDegreeMoreThanZero = 0;

	for (i = 0; i < BAM->rownr; i++) {
		degree = getDegree (BAM->bam[i], BAM->usercolnr, bp);
//		fprintf (stdout, "Degree pf node %d is: %d\n", i + 1, degree);
		NodesOfDegreeMoreThanZero[i] = degree;
		if (degree > 0)
			NrNodesOfDegreeMoreThanZero++;
	}

	return NrNodesOfDegreeMoreThanZero;
}

#endif /* GRAPHOPOPENMP_H_ */
