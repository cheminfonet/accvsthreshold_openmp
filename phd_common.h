// # v.1.0.1.1.0.1
// # first 3 digits refer to the phd_common version. The last three digits refers to the header file's own version
#define CB 10

void serror (int);


void serror (int value)
{
	printf ("\nERROR CODE: %3d\n\n", value);
	exit (value);
}

